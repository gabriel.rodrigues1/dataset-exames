export default [
  {id: '1', category: 'Positivo', color: '#0f9a00'},
  {id: '2', category: 'Negativo', color: '#a01414'},
  {id: '3', category: 'Incerto', color: '#cb9c00'},
];
